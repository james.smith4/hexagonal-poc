# Hexagonal architecture proof of concept

I have put together this example to explain how I see how a hexagonal approach to a Java/Spring service would look.

**Disclaimer**: this is my first attempt at a Java/Spring application - it will show. No thought has been put into error handling, response mapping, observability, etc. This is an attempt to show how a service will hang together using this architectural pattern. 

## Overview

![Hexagonal Architecture Overview](./images/Hexagonal%20architecture%20-%20Overview.jpg)

### Domain

- The domain layer defines the domain model that makes sense from a business perspective and to apply business rules against.
- The domain layer defines the adapter interfaces that it needs to be able to store data, interact with other services, interact with message brokers, etc. The domain layer passes the domain model into these interfaces. 
It does not care about the underlying implementation of each adapter model looks like (e.g. entities or protobuf contracts).
- With the hexagonal approach is it fairly simple to develop the domain layer in isolation using TDD and stubs/mocks for the adapters.

### Ports

- The ports encapsulate the application infrastructure. For example: gives it the behaviour of a rest API or gives another port the behaviour of a Kafka consumer
- Each port should handle wire validation. For example, has a mandatory field been provided, has a proper date been passed where a date is expected. 
- The port is responsible for mapping the wire DTO to the domain model. 
- Each port can wire up adapter implementations that make sense for it. Let's use a contrived (very contrived) example, we have two ports a REST server for external customers to create transactions and gRPC server for internal administrators to create transactions on the 
behalf of our customers. Both require the same domain logic however the REST endpoint will publish webhooks for the customer to consume but the gRPC endpoint will publish SQS messages for our internal application to consume. In this case the REST endpoint
can wire up an adapter that knows how to send webhooks while the gRPC server wires up an adapter than knows how to send messages of SQS. 

### Adapters

- Adapters implement the adapter interfaces defined by the domain layer.
- The adapters encapsulate the model required for the underlying infrastructure. For example, entities for a database or protobuf contracts for messages. 
- The adapter model or entire adapter infrastructure can be changes about without affecting the domain

# Running the application

## Database seeding

Database is seeded with a single account. The account_id is: `1a82bcc0-e028-4303-9326-79e8035aee82`

Additional accounts can be seeded by adding to the `/hexagonal-poc/src/main/resources/data.sql` script

## Running locally
```
mvn spring-boot:run
```

## Example Payload
```
{
  "id": "83180642-5df7-4a4a-a879-554588d136cf",
  "beneficiary": { 
    "name": "James Bene",
    "iban": "GB33BUKB20201555555555"
  },
  "amount": 100
}
```

## Example curl
curl -X POST -H "Content-Type: application/json" -d '{"id": "83180642-5df7-4a4a-a879-554588d136cf","beneficiary": {"name": "James Bene","iban": "GB33BUKB20201555555555"},"amount": 100}' http://localhost:8080/1a82bcc0-e028-4303-9326-79e8035aee82/transaction

# Layered architecture discussion 

I thought I would layout how I think a hexagonal approach could work for some of the discussion points located [here](https://github.com/apwiegman/layered-arch-discussion/blob/main/READ_ME.md)

**service to service call. An example is shown in transaction service where we need to call the fee service to calculate the fee for that specific transaction. 
Service to service calls return a DTO per previous weeks conversations - Is this correct? If a service needs an entity and not a DTO it goes via the repository 
(also shown in transaction service with retrieval of a bank account entity)?**

For a service to service call would be handled by an adapter. The domain would define a domain model to model the data it needed and then an interface for working getting that data. 
The adapter encapsulates the DTOs required for communicating between the adapter and the external service. It handles the mapping from the adapter DTO into the domain model.

**Mapping for DTOs - where should these mappers reside. There may be better ways of doing mapping - happy to hear suggestions.**

Mapping of ports DTOs into domain models takes place in the port layer (controller for REST service).
Mapping of adapter DTOs into domain models takes place in the adapter layer.
I expect there is a more Java idiomatic way of doing this that I have done. Rust defines From and Into traits (kinda like an interface). 
Implementing this trait on your DTO/Model structs was the idiomatic way of mapping between two structs.

**Schedulers - where would they sit. I've set up a example scheduler for sake of discussion.**

I may be misunderstanding the meaning of schedulers in this context but if we mean something that does something on a time schedule (every x minutes, at this time, etc.) then short answer is I see this being 
part of the port layer. I'll save my long answer for another day as it doesn't add anything to this discussion.

**Async consumers (Kafka or SQS pollers). I've put them in the api folders cause they are technically async apis. Thoughts/ what do layered gurus say? Should they rather go in clients although I like to see clients as something we depend on and call out to.**

These are just different ports in my opinion. We may have different ports use the same domain. If we split the projects up so we output different binaries we can effectively deploy each port as a seperate container in it's own K8s POD so effectively a seperate microservice.
