package com.hexagonal.hexagonalpoc.ports.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ParticipantDto {
    @JsonProperty(required = true)
    private String name;
    @JsonProperty(required = true)
    private String iban;

    public ParticipantDto(String name, String iban) {
        this.name = name;
        this.iban = iban;
    }
}
