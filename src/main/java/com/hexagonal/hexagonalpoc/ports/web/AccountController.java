package com.hexagonal.hexagonalpoc.ports.web;

import com.hexagonal.hexagonalpoc.domain.interfaces.IAccountService;
import com.hexagonal.hexagonalpoc.domain.models.Participant;
import com.hexagonal.hexagonalpoc.domain.models.Transaction;
import com.hexagonal.hexagonalpoc.domain.models.TransactionStatus;
import com.hexagonal.hexagonalpoc.ports.web.dto.TransactionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
public class AccountController {

    private final IAccountService accountService;

    @Autowired
    public AccountController(IAccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping(value = "/{accountId}/transaction", consumes = "application/json", produces = "application/json")
    ResponseEntity<String> initiateTransaction(@PathVariable(name="accountId") String accountId, @RequestBody TransactionDto transactionDto) {

        Transaction transaction = new Transaction(
                transactionDto.getId(),
                new Participant(
                        transactionDto.getBeneficiary().getName(),
                        transactionDto.getBeneficiary().getIban()),
                        transactionDto.getAmount(),
                        TransactionStatus.BOOKED
        );

        this.accountService.initiateTransaction(UUID.fromString(accountId), transaction);

        return ResponseEntity.ok("Transaction Created");
    }
}

