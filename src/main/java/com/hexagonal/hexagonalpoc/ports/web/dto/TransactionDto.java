package com.hexagonal.hexagonalpoc.ports.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class TransactionDto {
    @JsonProperty(required = true)
    private UUID id;
    @JsonProperty(required = true)
    private ParticipantDto beneficiary;
    @JsonProperty(required = true)
    private long amount;

    public TransactionDto(UUID id, ParticipantDto beneficiary, long amount) {
        this.id = id;
        this.beneficiary = beneficiary;
        this.amount = amount;
    }
}

