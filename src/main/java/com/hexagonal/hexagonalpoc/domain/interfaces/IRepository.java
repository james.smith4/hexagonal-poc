package com.hexagonal.hexagonalpoc.domain.interfaces;

import com.hexagonal.hexagonalpoc.domain.models.Account;
import com.hexagonal.hexagonalpoc.domain.models.Transaction;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

public interface IRepository {
    Optional<Account> getAccount(UUID accountId);

    Account updateAccount(Account account);

    Transaction createTransaction(Account account, Transaction transaction);
}
