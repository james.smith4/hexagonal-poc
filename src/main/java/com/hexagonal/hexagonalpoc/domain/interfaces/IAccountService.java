package com.hexagonal.hexagonalpoc.domain.interfaces;

import com.hexagonal.hexagonalpoc.domain.models.Transaction;

import java.util.UUID;

public interface IAccountService {
    void initiateTransaction(UUID accountId, Transaction transaction);
}
