package com.hexagonal.hexagonalpoc.domain.interfaces;

import com.hexagonal.hexagonalpoc.domain.models.Transaction;
import org.springframework.stereotype.Repository;

public interface IEventPublisher {
    void publishTransactionStateEvent(Transaction transaction);
}
