package com.hexagonal.hexagonalpoc.domain.services;

import com.hexagonal.hexagonalpoc.domain.interfaces.IAccountService;
import com.hexagonal.hexagonalpoc.domain.interfaces.IEventPublisher;
import com.hexagonal.hexagonalpoc.domain.interfaces.IRepository;
import com.hexagonal.hexagonalpoc.domain.models.Account;
import com.hexagonal.hexagonalpoc.domain.models.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class AccountService implements IAccountService {

    private IEventPublisher eventPublisher;
    private IRepository repository;

    @Autowired
    public AccountService(IEventPublisher eventPublisher, IRepository repository) {
        this.eventPublisher = eventPublisher;
        this.repository = repository;
    }

    @Override
    public void initiateTransaction(UUID accountId, Transaction transaction) {
        Optional<Account> optionalAccount = repository.getAccount(accountId);

        if (!optionalAccount.isPresent()) {
            // Do some error handling
        }

        Account account = optionalAccount.get();
        account.bookTransaction(transaction);

        repository.createTransaction(account, transaction);
        repository.updateAccount(account);
        eventPublisher.publishTransactionStateEvent(transaction);
    }
}
