package com.hexagonal.hexagonalpoc.domain.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.UUID;

@Getter
public class Transaction {
    @Id
    private UUID id;
    private Participant beneficiary;
    private long amount;
    private TransactionStatus status;

    public Transaction(final UUID id, final Participant beneficiary, final long amount, TransactionStatus status) {
        this.id = id;
        this.beneficiary = beneficiary;
        this.amount = amount;
        this.status = status;
    }

    public String toString() {
        return "Id: " + this.id + "Amount: " + this.amount + "Bene: " + this.beneficiary.toString();
    }
}
