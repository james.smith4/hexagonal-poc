package com.hexagonal.hexagonalpoc.domain.models;

public enum TransactionStatus {
    BOOKED {
        @Override
        public TransactionStatus nextState() {
            return TransactionStatus.SUBMITTED;
        }

        @Override
        public TransactionStatus failedState() {
            return TransactionStatus.BOOKING_FAILED;
        }
    },
    BOOKING_FAILED {
        @Override
        public TransactionStatus nextState() {
            return TransactionStatus.BOOKING_FAILED;
        }

        @Override
        public TransactionStatus failedState() {
            return TransactionStatus.BOOKING_FAILED;
        }
    },
    SUBMITTED {
        @Override
        public TransactionStatus nextState() {
            return TransactionStatus.SETTLED;
        }

        @Override
        public TransactionStatus failedState() {
            return TransactionStatus.FAILED;
        }
    },
    SETTLED {
        @Override
        public TransactionStatus nextState() {
            return TransactionStatus.SETTLED;
        }

        @Override
        public TransactionStatus failedState() {
            return TransactionStatus.FAILED;
        }
    },
    FAILED {
        @Override
        public TransactionStatus nextState() {
            return TransactionStatus.FAILED;
        }

        @Override
        public TransactionStatus failedState() {
            return TransactionStatus.FAILED;
        }
    };

    public abstract TransactionStatus nextState();
    public abstract TransactionStatus failedState();
}


