package com.hexagonal.hexagonalpoc.domain.models;

import lombok.Getter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.UUID;

@Getter
public class Account {
    @Id
    @Column(updatable = false, nullable = false)
    private UUID id;
    private String firstName;
    private String lastName;
    private String iban;
    private long currentBalance;
    private long availableBalance;
    private ArrayList<Transaction> transactions;
    private AccountStatus status;

    public Account(final UUID id, final String firstName, final String lastName, final String iban, long currentBalance,
                   long available_balance, ArrayList<Transaction> transactions, AccountStatus status) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.iban = iban;
        this.currentBalance = currentBalance;
        this.availableBalance = available_balance;
        this.transactions = transactions;
        this.status = status;
    }

    public void bookTransaction(Transaction transaction) {
        if (availableBalance - transaction.getAmount() < 0) {
            // Do some error handling
        }

        this.transactions.add(transaction);
        this.availableBalance = this.availableBalance - transaction.getAmount();
        System.out.println("*** Account: transaction booked");
    }

    public String getFullName() {
        return this.firstName + ' ' + this.lastName;
    }
}
