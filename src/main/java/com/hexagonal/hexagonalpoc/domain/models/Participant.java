package com.hexagonal.hexagonalpoc.domain.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Getter
public class Participant {
    private String name;
    private String iban;

    public Participant(final String name, final String iban) {
        this.name = name;
        this.iban = iban;
    }

    public String toString() {
        return "Name: " + this.name + "IBAN: " + this.iban;
    }
}
