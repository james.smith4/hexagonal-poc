package com.hexagonal.hexagonalpoc.domain.models;

public enum AccountStatus {
    PENDING,
    OPEN,
    CLOSED,
}
