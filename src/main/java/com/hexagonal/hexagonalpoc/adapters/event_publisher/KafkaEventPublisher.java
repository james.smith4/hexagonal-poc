package com.hexagonal.hexagonalpoc.adapters.event_publisher;

import com.hexagonal.hexagonalpoc.domain.interfaces.IEventPublisher;
import com.hexagonal.hexagonalpoc.domain.models.Transaction;
import org.springframework.stereotype.Component;

@Component
public class KafkaEventPublisher implements IEventPublisher {

    @Override
    public void publishTransactionStateEvent(Transaction transaction) {
        /// Map into an event data structure and serialise (e.g. protobuf message)
        /// Publish to Kafka topic
        System.out.println("*** KafkaEventPublisher: publishing TransactionStateEvent ");
    }
}
