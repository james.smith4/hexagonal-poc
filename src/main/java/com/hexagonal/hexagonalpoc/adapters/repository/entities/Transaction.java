package com.hexagonal.hexagonalpoc.adapters.repository.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "transaction", uniqueConstraints = {@UniqueConstraint(columnNames = {"id"})})
public class Transaction {
    @Id
    private UUID id;
    private String beneficiaryName;
    private String beneficiaryIban;
    private String remitterName;
    private String remitterIban;
    private long amount;
    private TransactionStatus status;
}