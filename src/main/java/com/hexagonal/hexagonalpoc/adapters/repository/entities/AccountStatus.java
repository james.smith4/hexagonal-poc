package com.hexagonal.hexagonalpoc.adapters.repository.entities;

public enum AccountStatus {
    PENDING,
    OPEN,
    CLOSED,
}