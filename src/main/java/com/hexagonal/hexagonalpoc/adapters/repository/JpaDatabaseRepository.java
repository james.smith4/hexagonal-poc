package com.hexagonal.hexagonalpoc.adapters.repository;

import com.hexagonal.hexagonalpoc.adapters.repository.repos.AccountRepository;
import com.hexagonal.hexagonalpoc.adapters.repository.repos.TransactionRepository;
import com.hexagonal.hexagonalpoc.domain.interfaces.IRepository;
import com.hexagonal.hexagonalpoc.domain.models.Account;
import com.hexagonal.hexagonalpoc.domain.models.AccountStatus;
import com.hexagonal.hexagonalpoc.domain.models.Transaction;
import com.hexagonal.hexagonalpoc.domain.models.TransactionStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

@Component
public class JpaDatabaseRepository implements IRepository {

    private final AccountRepository accountRepository;
    private final TransactionRepository transactionRepository;

    @Autowired
    public JpaDatabaseRepository(AccountRepository accountRepository, TransactionRepository transactionRepository) {
        this.accountRepository = accountRepository;
        this.transactionRepository = transactionRepository;
    }

    @Override
    public Optional<Account> getAccount(UUID accountId) {
        /// Get account entities from database
        /// Map into Account domain object
        System.out.println("*** JpaDatabaseRepository: getting account from database");

        var accountEntity = accountRepository.findById(accountId);

        if (accountEntity.isEmpty()) {
            return Optional.empty();
        }

        var unwrappedAccountEntity = accountEntity.get();

        return Optional.of(new Account(unwrappedAccountEntity.getId(),
                unwrappedAccountEntity.getFirstName(),
                unwrappedAccountEntity.getLastName(),
                unwrappedAccountEntity.getIban(),
                unwrappedAccountEntity.getCurrentBalance(),
                unwrappedAccountEntity.getAvailableBalance(),
                new ArrayList<Transaction>(),
                toDomainAccountStatus(unwrappedAccountEntity.getStatus()))
        );
    }

    @Override
    public Account updateAccount(Account account) {
        /// Map Account domain object (updateable fields) to entity
        /// Write to database
        var accountEntity = accountRepository.findById(account.getId());

        if (accountEntity.isEmpty()) {
            /// Some error handling
        }

        var unwrappedAccountEntity = accountEntity.get();
        unwrappedAccountEntity.setAvailableBalance(account.getAvailableBalance());
        unwrappedAccountEntity.setCurrentBalance(account.getAvailableBalance());
        unwrappedAccountEntity.setStatus(toEntityAccountStatus(account.getStatus()));

        accountRepository.save(unwrappedAccountEntity);
        return account;
    }

    @Override
    public Transaction createTransaction(Account account, Transaction transaction) {
        /// Map Transaction domain object to entity
        /// Write to database
        System.out.println("*** HibernateDatabaseRepository: inserting transaction into database");

        com.hexagonal.hexagonalpoc.adapters.repository.entities.Transaction transactionEntity = new com.hexagonal.hexagonalpoc.adapters.repository.entities.Transaction();
        transactionEntity.setId(transaction.getId());
        transactionEntity.setBeneficiaryName(transaction.getBeneficiary().getName());
        transactionEntity.setBeneficiaryIban(transaction.getBeneficiary().getIban());
        transactionEntity.setRemitterName(account.getFullName());
        transactionEntity.setRemitterIban(account.getIban());
        transactionEntity.setAmount(transaction.getAmount());
        transactionEntity.setStatus(toEntityTransactionStatus(transaction.getStatus()));


        transactionRepository.save(transactionEntity);
        return transaction;
    }

    private AccountStatus toDomainAccountStatus(com.hexagonal.hexagonalpoc.adapters.repository.entities.AccountStatus entityAccountStatus) {
        AccountStatus domainStatus;

        switch (entityAccountStatus) {
            case CLOSED:
                domainStatus = AccountStatus.CLOSED;
                break;
            case PENDING:
                domainStatus = AccountStatus.PENDING;
                break;
            default:
                domainStatus = AccountStatus.OPEN;
                break;
        }

        return domainStatus;
    }

    private com.hexagonal.hexagonalpoc.adapters.repository.entities.AccountStatus toEntityAccountStatus(AccountStatus accountStatus) {
        com.hexagonal.hexagonalpoc.adapters.repository.entities.AccountStatus entityStatus;

        switch (accountStatus) {
            case CLOSED:
                entityStatus = com.hexagonal.hexagonalpoc.adapters.repository.entities.AccountStatus.CLOSED;
            case PENDING:
                entityStatus = com.hexagonal.hexagonalpoc.adapters.repository.entities.AccountStatus.PENDING;
            default:
                entityStatus = com.hexagonal.hexagonalpoc.adapters.repository.entities.AccountStatus.OPEN;
        }

        return entityStatus;
    }

    private com.hexagonal.hexagonalpoc.adapters.repository.entities.TransactionStatus toEntityTransactionStatus(TransactionStatus transactionStatus) {
        com.hexagonal.hexagonalpoc.adapters.repository.entities.TransactionStatus entityStatus;

        switch (transactionStatus) {
            case BOOKED:
                entityStatus = com.hexagonal.hexagonalpoc.adapters.repository.entities.TransactionStatus.BOOKED;
                break;
            case BOOKING_FAILED:
                entityStatus = com.hexagonal.hexagonalpoc.adapters.repository.entities.TransactionStatus.BOOKING_FAILED;
                break;
            case SUBMITTED:
                entityStatus = com.hexagonal.hexagonalpoc.adapters.repository.entities.TransactionStatus.SUBMITTED;
                break;
            case SETTLED:
                entityStatus = com.hexagonal.hexagonalpoc.adapters.repository.entities.TransactionStatus.SETTLED;
                break;
            default:
                entityStatus = com.hexagonal.hexagonalpoc.adapters.repository.entities.TransactionStatus.FAILED;
                break;
        }

        return entityStatus;
    }
}
