package com.hexagonal.hexagonalpoc.adapters.repository.entities;

public enum TransactionStatus {
    BOOKED,
    BOOKING_FAILED,
    SUBMITTED,
    SETTLED,
    FAILED;
}
