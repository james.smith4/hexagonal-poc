package com.hexagonal.hexagonalpoc.adapters.repository.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "account", uniqueConstraints = {@UniqueConstraint(columnNames = {"id"})})
public class Account {
    @Id
    @Column(updatable = false, nullable = false)
    @Type(type="org.hibernate.type.UUIDCharType")
    private UUID id;
    private String firstName;
    private String lastName;
    private String iban;
    private long currentBalance;
    private long availableBalance;
    private AccountStatus status;
}
