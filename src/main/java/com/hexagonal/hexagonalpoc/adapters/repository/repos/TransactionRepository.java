package com.hexagonal.hexagonalpoc.adapters.repository.repos;

import com.hexagonal.hexagonalpoc.adapters.repository.entities.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, UUID> {
}
